<?php

function index(){
    $data['title']='关于我们';
    $data['csspath']=RES.'css/about.css';
     $result=find('settab','value',array('name'=>'关于我们'));
     $data['banner']=$result['value'];
    
    $cateid=find("category",'id',array('name'=>'关于我们'));
    $category=getCategory($cateid['id']);
    $data['category']=$category;
    $data['currcate']=empty($_GET['currcate'])?$category[0]['id']:$_GET['currcate'];

    $row=find('about','content',array('category'=> $data['currcate']));
    $data['content']=$row['content'];  

    view('site','about','index',$data);

}