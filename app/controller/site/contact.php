<?php

function index(){
    $data['title']='联系我们';
    $data['csspath']=RES.'css/contact.css';
    $result=find('settab','value',array('name'=>'联系我们'));
    $data['banner']=$result['value'];

    $arr=array('tel','addr','contact','mail');
    foreach($arr as $v){
        $result=find('settab','value',array('name'=>$v));
        $data[$v]=$result['value'];
    }

  
    view('site','contact','index',$data);
}