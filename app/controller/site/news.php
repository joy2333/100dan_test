<?php 

function index(){
    $arr=select('category','id,name',array('pid'=>1));
    $category=empty($_GET['category'])?$arr[0]['id']:$_GET['category'];
    $data=getpage('news','time Desc',5,5,array('category'=>$category));
    $data['title']='新闻中心';
    $data['csspath']=RES.'css/news.css';
    $result=find('settab','value',array('name'=>'新闻中心'));
    $data['banner']=$result['value'];
    $data['url']=$GLOBALS['url'];	
    $data['category']=$arr;
    $data['currcate']=$category;
    view('site','news','index',$data);


}

function details(){
    if(!empty($_GET['id'])){
        $id=$_GET['id'];

        $do=empty($_GET['do'])?'':$_GET['do'];
        if($do=="prev"){
             $row=find('news','',array('id <'=>$id),'id Desc');
            if(count($row)==0){
                 $row=find('news','','','id Asc');
             }
        }elseif($do=="next"){
             $row=find('news','',array('id >'=>$id),'id Asc');
             if(count($row)==0){
                 $row=find('news','','','id Desc');
             }
        }else{
            $row=find('news','',$id);
        }       
       
        $data['title']='新闻详情';
        $data['csspath']=RES.'css/news-detail.css';
        $result=find('settab','value',array('name'=>'新闻中心'));
        $data['banner']=$result['value'];
        $data['url']=$GLOBALS['url'];	
        $data['category']=select('category','id,name',array('pid'=>1));
        
        $data['row']=$row;
        view('site','news','details',$data);
    }
}


