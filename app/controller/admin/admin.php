<?php

 checkLogin();  
function index(){
           
	$data['title']='管理员';
	if(!empty($_GET['curpage'])){
		$data['curpage']=$_GET['curpage'];
	}else{
		$data['curpage']=1;
	}
    view('admin','admin','index',$data);


}

function gettab(){
	
	$data= getpage('admin', 'id Desc',8);   
	
    view('admin','admin','gettab',$data,false);

}

function add(){
   
	 
	if(!empty($_POST['username'])){
		$_POST['password']=md5($_POST['password']);		 
		$_POST['level']=json_encode($_POST['level']);			 
		if(insert('admin')){
			jump("index");
		}else{
			echo "<script>alert('添加用户失败！');</script>";
		}
	}

	 
	 
    view('admin','admin','add',['title'=>'添加管理员','levelarr'=>getMenus()]);

}

function edit(){
	 
	if(!empty($_GET['id'])){
		 
		$id=$_GET['id'];
		$curpage=isset($_GET['curpage'])?$_GET['curpage']:1;
		$row=find('admin','*',$id);
		if(!empty($_POST)&&is_numeric($id)){
			
			$_POST['level']=json_encode($_POST['level']);
			if(update('admin',$id)){
				jump('index',"curpage=$curpage");
			}else{
				echo "<script>alert('修改用户失败！');</script>";
			}

		}else{
			$data['title']='编辑管理员';
			$data['row']=$row;
			$data['levelarr']=getMenus();
			
			view('admin','admin','edit',$data);
		}



	}
}

function del(){	
	if(!empty($_GET['id'])){
		$idarr=explode('|',$_GET['id']);
		foreach($idarr as $id){
			if(is_numeric($id)){
				delete('admin',$id);
			}
		}
	}
	$curpage=isset($_GET['curpage'])?$_GET['curpage']:1;
	jump('index',"curpage=$curpage");
}

function editpwd(){
	

	if(!empty($_POST['pwd'])){
		$pwd=md5($_POST['pwd']);
		$password=md5($_POST['password']);
		$repassword=md5($_POST['repassword']);
		$row=find('admin','password',array('username'=>$_SESSION['username']));
		if(($pwd==$row['password'])&&($password==$repassword)){
			$_POST['password']=$password;
			if(update('admin',array('username'=>$_SESSION['username']))){
				jump('index');
			}else{
				echo "<script>alert('修改用户失败！');</script>";
			}
		}else{
			echo "<script>alert('原密码错误或两次输入的新密码不一样！');</script>";

		}
	}
	view('admin','admin','editpwd',['title'=>'修改密码']);


}

function ckusername(){
	if(!empty($_POST['param'])){
		if(!empty($_GET['id'])){
			$row=find('admin','',$_GET['id']);
			if($row){
				if($_POST['param']==$row['username']){
					echo 'y';
					die;
				}


			}
		}
		
		$total=total('admin',array('username'=>$_POST['param']));			
		if($total>0){
			echo '该用户名已经存在。';				
		}else{
			echo 'y';
		}		
	}
}

function ckmail(){
	if(!empty($_POST['param'])){
		if(!empty($_GET['id'])){
			$row=find('admin','',$_GET['id']);
			if($row){
				if($_POST['param']==$row['mail']){
					echo 'y';
					die;
				}
			}
		}
		
		if(!empty($_GET['t'])&&$_GET['t']=='set'){
			$row=find('admin','',array('username'=>$_SESSION['username']));
			if($row){
				if($_POST['param']==$row['mail']){
					echo 'y';
					die;
				}
			}
		}		
	 
		$total=total('admin',array('mail'=>$_POST['param']));			
		if($total>0){
			echo '该邮箱已经存在。';				
		}else{
			echo 'y';
		}		
	}
}

function cset(){

	if(!empty($_POST)){
		foreach($_POST as $k=>$v){
			$_POST['name']=$k;
			$_POST['value']=$v;
			update('settab',array('name'=>$k));
		}
	}

	$data['title']='设置联系信息';
	$col=array('tel','mail','contact','addr','outbox','outboxpwd');
	foreach($col as $v){
		$arr=find('settab','value',array('name'=>$v));
		if($arr){
			$data[$v]=$arr['value'];
		}else{
				$data[$v]='';
		}

	}

	view('admin','admin','cset',$data);

}

function set(){
	$username=$_SESSION['username'];

	if(!empty($_POST)){
		if(update('admin',array('username'=>$username))){
			echo "<script>alert('您的信息已修改！')</script>";
		}else{
			echo "<script>alert('您的信息修改失败！')</script>";
		}
	}
	$row=find('admin','',array('username'=>$username));
	$row['title']='修改个人信息';
	view('admin','admin','set',$row);


}