<?php
 checkLogin(); 
 function index(){
     $data['title']='留言后台管理';
	if(isset($_GET['curpage'])){
		$data['curpage']=$_GET['curpage'];
	}else{
		$data['curpage']=1;
	}
    view('admin','message','index',$data);


 }

 function gettab(){
    $order='time Desc';
	$url="";
	if(!empty($_GET['order'])){
		$order='time Asc';
		$url="&order=asc";
	}

	$find=array();
	if(!empty($_GET['findstr'])){
		$find=array('message %'=>$_GET['findstr']);
		$url.="&findstr=".$_GET['findstr'];
	}

	
	$data= getpage('message', $order,8,5,$find);  
   // $data= getpage('message', 'time Desc',8);
   $data['addurl']=$url;
    view('admin','message','gettab',$data,false);

 }

 function show(){
    if(!empty($_GET['id'])){
        $row=find('message','',$_GET['id']);
        view('admin','message','show',array('row'=>$row,'title'=>'留言显示'));
    }

 }

 function del(){
    if(!empty($_GET['id'])){
        $id=$_GET['id'];
        $idarr=explode('|', $id);	
        $curpage=isset($_GET['curpage'])?$_GET['curpage']:1;        
        foreach($idarr as $i){
            if(is_numeric($i)){                
                    delete('message',$i);
            }            
        }
	    jump('index',"curpage=$curpage");
    }
 }


