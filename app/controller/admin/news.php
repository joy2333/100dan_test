<?php 
checkLogin();
function index(){   
      
	$data['title']='新闻页后台';
	if(isset($_GET['curpage'])){
		$data['curpage']=$_GET['curpage'];
	}else{
		$data['curpage']=1;
	}
    view('admin','news','index',$data);
}

function add(){
     
	if(isset($_POST['title'])){
		$_POST['time']=time();
		$imgarr=uploadImg('images', 'upload/news', $_POST['checkimages'],'',false);		 
		if(empty($imgarr['error'])){
			$_POST['image']=$imgarr['image'];
			
		}else{
			echo $imgarr['error'];
			die;
		}				 
		if(insert('news')){
			header('location:index.php?m=admin&c=news&a=index');			
			
		}else{
			echo "<script>alert('添加新闻没有成功哦！');</script>";
			
		}		
	}
	$category=getCategory(1);
	 view('admin','news','add',array('title'=>'添加新闻','category'=>$category));
}

function gettab(){
	$order='time Desc';
	$url="";
	if(!empty($_GET['order'])){
		$order='time Asc';
		$url="&order=asc";
	}

	$find=array();
	if(!empty($_GET['findstr'])){
		$find=array('content %'=>$_GET['findstr']);
		$url.="&findstr=".$_GET['findstr'];
	}

	
	$data= getpage('news', $order,8,5,$find);  
	foreach($data['rows'] as $k=>$row){
		$arr=find('category','name',$row['category']);
		$data['rows'][$k]['name']=$arr['name'];
	}

	$data['addurl']=$url;
 
    view('admin','news','gettab',$data,false);

}

function del(){
    
    if(isset($_GET['id'])){
        $id=$_GET['id'];
        $idarr=explode('|', $id);	
        $curpage=isset($_GET['curpage'])?$_GET['curpage']:1;	
        foreach($idarr as $i){
            $row=find('news','image',$i);
            if($row){
                if(file_exists($row['image'])){
                    unlink($row['image']);
                }
            }		 
        } 
        $result=delete('news',$idarr);
        if($result){		
            jump('index',"curpage=$curpage");
        }else{		
            echo "<script>alert('新闻没有成功删除哦！');</script>";	
        }	
    }

}

function edit(){
       
	if(isset($_GET['id'])){
		$id=$_GET['id'];
		$curpage=isset($_GET['curpage'])?$_GET['curpage']:1;
		$row=find('news','*',$id);
		if(isset($_POST['title'])&&is_numeric($id)){			 
			
			if($row){

				// 如果有上传新的图片，就删除原来的图片
				if(isset( $_POST['checkimages'])){			
					$imgs=uploadImg('images', 'upload/news', $_POST['checkimages'],'',false);		 
					if(empty($imgs['error'])){
						if(file_exists($row['image'])){
							unlink($row['image']);
						}
						$_POST['image']=$imgs['image'];


					}else{
						echo $imgs['error'];
						die;
					}			 
				}
				
				//如果用户删除了之前上传的图片
				if(empty($_POST['uploadedimages'])){
					if(file_exists($row['image'])){
						unlink($row['image']);
					}
					$_POST['image']='';

				}
			}
			
			$result=update('news', $id);
			if($result){				
				
				jump('index',"curpage=$curpage");		
			}else{
				echo "<script>alert('修改新闻没有成功哦！');</script>";			
			}
		}else{
 
			$row=find('news','*',$id);
			$category=getCategory(1);
			if(count($row)>0){
				$data['row']=$row;
				$data['title']='编辑新闻';
				$data['category']=$category;
				view('admin','news','edit',$data);
				
			}else{
				echo "<script>alert('没有查询到此ID的新闻，可能已经删除了！');</script>";	
				header("location:index.php?m=admin&c=news&a=index&curpage=$curpage");
			}			
		}		
	}
	

}

function checktop(){
	if(!empty($_POST)){
		$id=$_POST['id'];
		unset($_POST['id']);
		if(is_numeric($id)){
			if(update('news',$id)){
				echo "1";
			}else{
				echo "修改失败！";
			}
		}
	}

}