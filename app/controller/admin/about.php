<?php
 checkLogin(); 

function index(){
    $data['title']='公司介绍后台管理';
	if(isset($_GET['curpage'])){
		$data['curpage']=$_GET['curpage'];
	}else{
		$data['curpage']=1;
	}
    view('admin','about','index',$data);
}

function gettab(){
	 $data= getpage('about', 'time Desc',8);   
	
	foreach($data['rows'] as $k=>$row){
		$arr=find('category','name',$row['category']);
		$data['rows'][$k]['name']=$arr['name'];
	}

    view('admin','about','gettab',$data,false);
 }

function add(){
	if(!empty($_POST)){
		$_POST['time']=time();
		if(insert('about')){
			jump('index');	
			
		}else{
			echo "<script>alert('添加介绍没有成功哦！');</script>";
			
		}	
	}
	 $category=getCategory(9);
	 view('admin','about','add',array('title'=>'添加介绍','category'=>$category));
}

function edit(){
	if(!empty($_GET['id'])){
		$id=$_GET['id'];
		$row=find('about','',$id);
		if(!empty($_POST)){
			$_POST['time']=time();
			$imgarr=array('image/gif','image/jpg','image/png','image/jpeg');
			if(!empty($_FILES['editfile']['name'])){
				if(in_array($_FILES['editfile']['type'],$imgarr)){
					$path='upload/about/'.date('Ym',time());
					if(!file_exists($path)){
						mkdir($path,0777,true);

					}
					$path.='/'.time().rand(10,99).$_FILES['editfile']['name'];
					move_uploaded_file($_FILES['editfile']['tmp_name'],$path);
					$_POST['image']=$path;
					if(file_exists($row['image'])){
						unlink($row['image']);
					}


				}
			}
			//print_r($_POST);die;
			if(update('about',$id)){
				jump('index');	
				
			}else{
				echo "<script>alert('修改介绍没有成功哦！');</script>";
				
			}	
		}
		
		$curpage=isset($_GET['curpage'])?$_GET['curpage']:1;
		$category=getCategory(9);
		if(count($row)>0){
			$data['row']=$row;
			$data['title']='编辑介绍';
			$data['category']=$category;
			view('admin','about','edit',$data);
			
		}else{
			echo "<script>alert('没有查询到此ID的关于公司的介绍，可能已经删除了！');</script>";	
			jump('index',"curpage=$curpage");
		}			

	}


}