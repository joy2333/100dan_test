<?php
/**
 * mysqli封装文件
 * 付志飞  2017-07-26
 */
 
 
 //执行SQL语名并返回结果
 function query($str){
 	
 	$conn=mysqli_connect(DBHOST, DBUSER, DBPWD, DBNAME, DBPORT)or die(mysqli_connect_error());
	mysqli_query($conn,"SET NAMES UTF8");
	$rs=mysqli_query($conn,$str);
	if(!$rs){
		die(mysqli_error($conn).'<br />'.$str);
	}

	mysqli_close($conn);
	return $rs;
	
 }


function getWhere($where){
	$wArr=[];
	if(is_array($where)){//是数组
		if( array_keys($where) !== range(0, count($where) - 1)){  //是关联数组
			foreach($where as $k=>$v){
				$v=addslashes($v);
				$tmp=substr($k,-1);
				switch($tmp){
					case ">":
					case "<":	
					case '=':	
						$wArr[]="({$k}'{$v}')";
						break;
					case '%':
						$k=substr($k,0,strlen($k)-1);	
						$wArr[]="({$k} LIKE '%{$v}%')";
						break;
					default:	
						$wArr[]="({$k}='{$v}')";
						break;
						
				}
				
			}
			 
		}else{
			$wArr[]='`id` in(\''.join("','", $where).'\')';
		}
	}else{
		$wArr[]="`id`='{$where}'";
	}
	return join('AND',$wArr);
	
}


function getTableCols($table){
	$coltb=query("show columns from `{$table}`");
	$cols=[];
	if($coltb){
		while($tb=mysqli_fetch_assoc($coltb)){
			$cols[]=$tb['Field'];
		}
	}
	
	if(count($cols)>0){		
		return $cols;		
	}
	return false;
}

function select($table,$col='*',$where=null,$order='',$limit=''){
	$w=$where?'WHERE '.getWhere($where):'';
	$order=!empty($order)?"ORDER BY {$order}":'';
	$limit=!empty($limit)?"LIMIT {$limit}":'';
	$col=!empty($col)?$col:'*';
	$sql="SELECT {$col} FROM `{$table}` {$w} {$order} {$limit}";
	 
 	$data=[];
	$rs= query($sql);
	while($r=mysqli_fetch_assoc($rs)){
		$data[]=$r;
	}
	
	return $data;
}

function distinct($table,$col,$where=null,$order='',$limit=''){
	$w=$where?'WHERE '.getWhere($where):'';
	$order=!empty($order)?"ORDER BY {$order}":'';
	$limit=!empty($limit)?"LIMIT {$limit}":'';	
	$sql="SELECT DISTINCT `{$col}` FROM `{$table}` {$w} {$order} {$limit}";
	 
 	$data=[];
	$rs= query($sql);
	while($r=mysqli_fetch_assoc($rs)){
		$data[]=$r;
	}
	
	return $data;

}

/**
*联表查询
*/
function selectJoin($table1,$table2,$on1,$on2,$col1='*',$col2='*',$where=null,$order='',$limit='',$jointype='inner'){
	$w=$where?'WHERE '.getWhere($where):'';
	$order=!empty($order)?"ORDER BY {$order}":'';
	$limit=!empty($limit)?"LIMIT {$limit}":'';

	$colarr1=empty($col1)?['*']:explode(',',$col1);
	$colarr2=empty($col2)?['*']:explode(',',$col2);

	$col1='';
	$col2='';

	foreach($colarr1 as $arr){
		$col1.="`{$table1}`.{$arr},";
	}	
	foreach($colarr2 as $arr){
		$col2.="`{$table2}`.{$arr},";
	}
	$col1=substr($col1,0,-1);
	$col2=substr($col2,0,-1);

	$on="`{$table1}`.`{$on1}`=`{$table2}`.`{$on2}`";
	$jointype=strtoupper($jointype);
	$sql="SELECT {$col1},{$col2} FROM `{$table1}` {$jointype} JOIN `{$table2}` ON {$on} {$w} {$order} {$limit}";
	echo $sql;

	$data=[];
	$rs= query($sql);
	while($r=mysqli_fetch_assoc($rs)){
		$data[]=$r;
	}
	
	return $data;
}

function total($table,$where=null){
	$w=$where?'WHERE '.getWhere($where):'';		
	$sql="SELECT count(*) AS count FROM `{$table}` {$w} "; 	 
	//echo $sql;
	$rs=query($sql);
	$row=mysqli_fetch_assoc($rs);
	return $row['count'];
	
}

function sum($table,$col,$where=null){
	$w=$where?'WHERE '.getWhere($where):'';		
	$sql="SELECT sum($col) AS sum FROM `{$table}` {$w} "; 	 
	//echo $sql;
	$rs=query($sql);
	$row=mysqli_fetch_assoc($rs);
	return $row['sum'];
	
}

function find($table,$col='*',$where=null,$order=''){
	$w=$where?'WHERE '.getWhere($where):'';	
	$order=!empty($order)?"ORDER BY {$order}":'';	
	$sql="SELECT * FROM `{$table}` {$w} {$order} LIMIT 1"; 	 
	$rs=query($sql);
	return mysqli_fetch_assoc($rs);
}

function insert($table){
	$cols=getTableCols($table);
	
	if($cols){
		$zdarr=[];
		$vaarr=[];
		foreach($_POST as $k=>$v){			
			if(in_array($k, $cols)){
				$v=addslashes($v);
				$zdarr[]=$k;
				$vaarr[]="'{$v}'";
			}		
		}
		
		if(count($zdarr)>0&&count($vaarr)>0){
			$sqlstr="INSERT INTO {$table}(".join(',', $zdarr).')VALUES('.join(',', $vaarr).')';			
			return query($sqlstr);
			 		
			
		}else{
			
			echo 'error ';
		}
		
			
	}
	return false;
	
}

function update($table,$where){
	$cols=getTableCols($table);
	$w='WHERE '.getWhere($where);
	if($cols){
		$str='';
		foreach($_POST as $k=>$v){
			if(in_array($k, $cols)){
				$v=addslashes($v);				
				$str.="{$k}='$v',";
			}		
		}		
		if($str!=''){
			$str=substr($str,0,strlen($str)-1);
			$sqlstr="UPDATE {$table} SET {$str} {$w}";
			return query($sqlstr);
						
		}else{			
			echo 'error ';
		}			
	}
	return false;	
}

function delete($table,$where){
	$w='WHERE '.getWhere($where);
	$sqlstr="DELETE FROM {$table} {$w}";
	return query($sqlstr);
		
}
