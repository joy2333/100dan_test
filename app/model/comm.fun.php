<?php 

	/**
	 * 跳转页面
	 * @param $url  string   模块/控制器/操作名 格式的路径字符串，可以写其中几个或不写
	 * @param $data  string   参数名	
	 * @return void
	 */
	function jump($url='',$data=''){
		global $m;
		global $c;
		global $a;

		if(!empty($data)){
			$data='&'.$data;
		}
		if($url==''){
			header("location:index.php?m={$m}&c={$c}&a={$a}{$data}");die;	
		}
		$arr=explode('/',$url);
		$len=count($arr);
		if($len==1){
			header("location:index.php?m={$m}&c={$c}&a={$arr[0]}{$data}");die;	
		}elseif($len==2){
			header("location:index.php?m={$m}&c={$arr[0]}&a={$arr[1]}{$data}");die;	
		}elseif($len==3){
			header("location:index.php?m={$arr[0]}&c={$arr[1]}&a={$arr[2]}{$data}");die;	
		} 
		
	}
	
	
	/**
	 * 显示视图
	 * @param $m  string   模块名称
	 * @param $c  string   控制器名称
	 * @param $a   string	操作名称	
	 * @param $data array   要显示到视图的数据
	 * @return void
	 */
	function view($m='site',$c='index',$a='index',$data=array(),$isincludeH=true){
		foreach($data as $k=>$v){
			$$k=$v;
		}
		if(isset($_SESSION['username'])){		
			$mylevel=find("admin",'level',array('username'=>$_SESSION['username']));
			$mylevelarr=json_decode($mylevel['level']);
			$menudata=[];			
			/* $menus=select('menu','',$mylevelarr);
			foreach($menus as $v){	
				if($v['hidden']==0){
					$menudata[$v['submenu']][]=$v;
				}
			}	 */

			foreach($mylevelarr as $mylevel){
				$arr=find('menu','',array('id'=>$mylevel,'hidden'=>0));
				if(!empty($arr)){
					$menudata[$arr['submenu']][]=$arr;
				}
				
			}
			//print_r($menudata);die;
			$currmenu=find('menu','submenu',array('module'=>$c,'app'=>$m,'action'=>$a));		 
		}
		if($isincludeH){
			include "app/view/$m/head.html";
			include "app/view/$m/$c/$a.html";
			include "app/view/$m/foot.html";
		}else{
			include "app/view/$m/$c/$a.html";
		}		
	}

	/**
	 * 分页,但不返回分页后的字符串
	 * @param $table  string  表名	 
	 * @param $order  string 定义排序
	 * @param $count   int  每页要显示的条数
	 * @param $btn_count  inte  要显示的按钮个数
	 * @param $where  要过虑的条件
	 * @return  array  由rows,page,curpge等组成的数组
	 */
	function getpage($table,$order,$count=5,$btn_count=5,$where=array()){
		
		$total=total($table,$where);  //总的新闻条数  
		$curpage=isset($_GET['curpage'])?$_GET['curpage']:1;  //当前是第几页	  
		$pages_count=ceil($total/$count);   //总共要分几页
				
		if($curpage>$pages_count){   //当获取到的当前页数大于总的页数时等于总的页数
			$curpage=$pages_count;
		}
		
		$prepage=$curpage-1;    //上一页数
		if($prepage<1){
			$prepage=1;
		}
		
		$nextpage=$curpage+1;   //下一页数
		if($nextpage>$pages_count){
			$nextpage=$pages_count;
		}
		
		$btn_start=$curpage-floor($btn_count/2);   //分页按钮开始的数
		$btn_end=$curpage+floor($btn_count/2);   //分页按钮结束时的数
		if($btn_start<1){     //当分页按钮开始的数小于1时，开始的数为1，结束的数是要显示的按钮数
			$btn_start=1;
			$btn_end=$btn_count;
		}
		
		if($btn_end>$pages_count){   //当分页按钮结束的数大于总的页数总数时，结束的数是页数总数，开始的数是页数总数-按钮数+1
			$btn_end=$pages_count;
			$btn_start=$pages_count-$btn_count+1;
		}
		
		if($pages_count<$btn_count){  //当总页数小于设置的要显示的按钮数时，结束的数是总页数，开始的数是1
			$btn_end=$pages_count;
			$btn_start=1;
			
		}   
		
		/*根据当前页数向数据库获取数据 开始*/
		if($curpage<1){   //当获取到的当前页小于1时，等于1
			$curpage=1;   		
		}
		$startIndex=($curpage-1)*$count;	
		
			$rows=select($table,'*',$where,$order,"{$startIndex},{$count}");
		/*根据当前页数向数据库获取数据  结束*/
		$data=array(
			'rows'=>$rows,	     //从数据库查询到的行数
			'curpage'=>$curpage,  //当前页
			'prepage'=> $prepage,  //上一页页码
			'nextpage'=>$nextpage,  //下一页页码
			'start'=>$btn_start,    //按钮开始的数字
			'end'=>$btn_end,     //按钮结束的数字
			'count'=>$pages_count   //总的页数
		);		
		return $data;			
	}



	/**
	 * 上传多张图片
	 * @param $name string input标签type是file的name属性值
	 * @param $path string 上传的图片要保存的位置,结尾不要“/”，会自动存放到当前年月组成的文件夹中
	 * @param $checkImgs array  确定要上传的图片的序号
	 * @param $waterImg  string  水印图片的位置,如果为空，就不添加水印
	 * @param $thumbimg  boolen  是否生成缩略图
	 * @return  array  返回由image thumb water组成的数组
	 */
	function uploadImg($name,$path,$checkImgs=array(),$waterImg='',$thumbimg=true){
		$imagePathArr=[];
		$thumbPathArr=[];
		$waterPathArr=[];
		$errorstr='';
		foreach($checkImgs as $v){
			$imgpath=$path.'/'.date('Ym').'/';
			if(!file_exists($imgpath)){
				mkdir($imgpath,0777,true);				
			}			
			$imageType=array("image/jpeg","image/gif","image/png","image/jpg");
			if(!empty($_FILES[$name]['name'][$v])){  //判断上传的文件中当前下标的name是不是空的
					if(in_array($_FILES[$name]['type'][$v],$imageType)){  //判断上传的文件是否是图片
						if($_FILES[$name]['size'][$v]<2*1024*1024){//判断上传的图片大小是否小于2M
							$imgpath=$imgpath.time().rand(1000,9999);
							$thumbPath=$imgpath.'_thumb_'.$_FILES[$name]['name'][$v];
							$waterPath=$imgpath.'_water_'.$_FILES[$name]['name'][$v];
							$imgpath=$imgpath.$_FILES[$name]['name'][$v];
							
							move_uploaded_file($_FILES[$name]['tmp_name'][$v], $imgpath);
							if($thumbimg){
								thumb($imgpath,$thumbPath);
							}else{
								$thumbpath='';
							}
							
							if(file_exists($waterImg)){
								water($imgpath,$waterPath,$waterImg);
								
							}else{
								$waterPath="";
							}
							$imagePathArr[]=$imgpath;
							$thumbPathArr[]=$thumbPath;
							$waterPathArr[]=$waterPath;
							
						}else{
							$errorstr='上传的图片大于2M';
							break;
						}
						
						
					}else{
						$errorstr='上传的不是图片';
						break;
					}
				
			}else{
				$errorsrt='上传的文件名为空';
				break;
				
			}
			
			
		}				
		 return array(
					'image'=>implode('|', $imagePathArr),
					'thumb'=>implode('|',$thumbPathArr),
					'water'=>implode('|',$waterPathArr),
					'error'=>$errorstr
					);
		
	}

	/**
	*删除保存在服务器中的多张图片
	*@param  $imgs array  要删除的图片数组
	*/
	function unlink_imgs($imgs){
		foreach($imgs as $filename){
			if(file_exists($filename)){
				unlink($filename);
			}
		}
		
	}

	/**
	*获取无极分类表
	*@param $pid string 要获取的顶层分类的pid
	*@param $delid string 要排除的id及其子类
	*@param $catearr array 返回的数组会包含此数组
	*@param  $level int  开始的层级
	*@return array
	*/
	function getCategory($pid=0,$delid=null,$catearr=[],$level=0){
		$arr=select('category','',array('pid'=>$pid));
		foreach($arr as $v){
			if($v['id']==$delid){
				continue;
			}
			$str='';
			for($i=0;$i<$level;$i++){
				$str.='___';
			}
			$v['level']='  '.$str;
			$catearr[]=$v;
			$catearr=getCategory($v['id'],$delid,$catearr,$level+1);

		}
		return $catearr;
	}

	/*为分类的的添加和编辑获取所需要的可操作类别*/
	function getMenus(){
		$menudata=[];
		$menus=select('menu','id,name',array('pid'=>0,'app'=>'admin'));
		foreach($menus as $v){			
			$menudata[$v['id'].'_'.$v['name']]=select('menu','',array('pid'=>$v['id']));

		} 
		//print_r($menudata);die;
		return $menudata;

	}

	function sendMail($address,$body){

		$row=find('settab','',array('name'=>'outbox'));
		$outbox=$row['value'];

		$row=find('settab','',array('name'=>'outboxpwd'));
		$outboxpwd=$row['value'];
		 
		if(!empty($outbox)&&(!empty($outboxpwd))){
		
			include("/app/model/phpmailer/class.phpmailer.php");
			$mail             = new PHPMailer();//实例化一个邮件发送类       
			$mail->IsSMTP(); // telling the class to use SMTP 使用smtp协议发送
			$mail->SMTPDebug  = 0;//错误调试：0表示不打开错误调试
			$mail->SMTPAuth   = true;
			$mail->CharSet    = "utf-8";
			$mail->Host       = "smtp.163.com"; // sets the SMTP server 设置发送邮件服务器，如：smtp.qq.com
			$mail->Port       = 25;                    // set the SMTP port for the GMAIL server 邮件发送服务的端口
			$mail->Username   = $outbox; // SMTP account username 发送邮件的邮箱用户名，如：123@qq.com
			$mail->Password   = $outboxpwd;        // SMTP account password 发送邮件的邮箱密码，如：123456，是123@qq.com的密码
			$mail->SetFrom($outbox, 'admin');//设置接收来源，如：123@qq.com
			$mail->AddReplyTo($outbox,"admin");//回复邮箱，如果别人按回复按钮，会直接指定的回复邮箱
			$mail->Subject    = "重设密码";//标题,主题
			$mail->MsgHTML($body);//内容使用html格式
		
			
			$mail->AddAddress($address, "用户组");//有多个邮箱地址，使用多次			 
			$mail->Send();

			echo 1;
		}else{
			echo 0;
		}
	}
	

	